import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'

export default function Post() {
    const [post, setPost] = useState({
        Email: "",
        Name: "",
        Password: "",
        Role: "",
    })



    const nav = useNavigate()
    // get value
    const handleChange = (event) => {
        let value = event.target.value
        setPost((prev) => {
            return {
                ...prev,
                [event.target.name]: value
            }
        })
    }

    // call post
    const handleOnclick = () => {
        fetch('https://vietcpq.name.vn/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/user/user', {
            method: 'POST',
            headers: {
                'Accept': "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(post)
        }).then(res => {
            if (res.status === 200) {
                nav('/')
            } else {
                alert("lỗi")
            }
        }).then(() => setPost({
            Email: "",
            Name: "",
            Password: "",
            Role: "",
        }))


    }
    return (
        <div>
            <input value={post.Email} name='Email' placeholder='Email' onChange={(event) => handleChange(event)}></input>
            <input value={post.Name} name='Name' placeholder='Name' onChange={(event) => handleChange(event)}></input>
            <input value={post.Password} name='Password' placeholder='Password' onChange={(event) => handleChange(event)}></input>
            <input value={post.Role} name='Role' placeholder='Role' onChange={(event) => handleChange(event)}></input>
            <button onClick={handleOnclick}>handleOnclick</button>
        </div>
    )
}
