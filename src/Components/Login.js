import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';

export default function Login() {
    let nav = useNavigate()
    const [user, setUser] = useState({ Email: "", Password: "" });
// get value input and setUser
    const handlChange = (event) => {
        let value = event.target.value
        let key = event.target.name
        setUser((prestate) => {
            return {
                ...prestate,
                [key]: value
            }
        })
    }
// click fetch API kiểm tra xem user có trong hệ thống không (user gồm Email và Password)
    const handleOnclick = () => {
        if (user.Email && user.Password) {
            fetch(`https://vietcpq.name.vn/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/user/Login`, {
                method: "POST",
                headers: {
                    'Accept': "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(user)
            })
                .then(res => {
                    // nếu có thì trả về status 200 và setItem 
                    if (res.status === 200) {
                        localStorage.setItem("User", JSON.stringify(user))
                        // nếu đăng nhập đúng quay lại path(/) lúc này Home sẽ được render
                        nav('/')
                    } else {
                        alert("SAI CAI GI DO")
                    }
                })

        }           
    }
    return (
        <div>

            <input placeholder='Email' name='Email' onChange={(event) => handlChange(event)} />
            <input placeholder='Password' name='Password' onChange={(event) => handlChange(event)} />
            <button onClick={handleOnclick}>handleOnclick</button>

        </div>
    )
}
