import React, { useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'

export default function Edit() {
    const [pass, setPass] = useState('')
    const [name, setName] = useState('')
    // detructoring get state chuyền vào thông qua navigate
    const { state } = useLocation()
    const nav = useNavigate()
    // change name
    const handlechangeName = (event) => {
        setName(event.target.value)
    }
    // mật khẩu bắt buộc đúng mới sửa tên được
    const handlechangePass = (event) => {
        setPass(event.target.value)
    }
    // requet
    const handleclick = () => {
        fetch('https://vietcpq.name.vn/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/user/user', {
            method: 'PUT',
            headers: {
                'Accept': "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                "Email": state.user.Email,
                "Name": name,
                "Password": pass
            })
        }).then(res => {
            if (res.status === 200) {
                alert("sửa thành công")
                nav("/")
            } else {
                alert("ko thành công")
            }
        })
    }
    return (
        <div>
            <p>ACCOUNT: {state.user.Email}</p>
            <label>Name:</label>
            <input value={name} onChange={event => handlechangeName(event)}></input>
            <br></br>
            <label>Password:</label>
            <input value={pass} onChange={event => handlechangePass(event)}></input>
            <br></br>
            <button onClick={handleclick}>CHANGE</button>
        </div>
    )
}
